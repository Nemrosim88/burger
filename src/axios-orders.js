import axios from "axios";

const instance = axios.create({
  baseURL: "https://react-my-burger-140af.firebaseio.com/"
});

export default instance;
