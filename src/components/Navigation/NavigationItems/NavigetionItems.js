import React from "react";
import cssClasses from "./NavigationItems.css";
import NavigationItem from "./NavigationItem/NavigationItem";

/**
 *  <NavigationItem link="" active>Burger Builder</NavigationItem>
 * Для boolean можно писать сокращщенно вместо active={true}
 * @param {*} props
 */
const navigationItems = props => (
  <ul className={cssClasses.NavigationItems}>
    <NavigationItem link="/" active>
      Burger Builder
    </NavigationItem>
    <NavigationItem link="/">Checkout</NavigationItem>
  </ul>
);

export default navigationItems;
