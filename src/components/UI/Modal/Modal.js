import React, { Component } from "react";
import cssClasses from "./Modal.css";
import Aux from "../../../hoc/auxjs/AuxFile";
import Backdrop from "../Backdrop/Backdrop";

class Modal extends Component {
  /**
   * boolean shouldComponentUpdate(object nextProps, object nextState)
   * Вызывается перед рендерингом при получени новых свойств или состояния.
   * Внесены следующие измененения: Только лишь когда в BurgerBuilder 'show' будет изменено ,
   *  будет происходить рендеринг компонента.
   *
   *
   * @param {*} nextProps
   * @param {*} nextState
   */
  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.show !== this.props.show ||
      nextProps.children !== this.props.children
    );
  }

  /**
   * Вызывается один раз, на клиенте и сервере, непосредственно перед началом рендеринга.
   */
  componentWillUpdate() {
    console.log("[Modal] WillUpdate");
  }

  render() {
    return (
      <Aux>
        <Backdrop show={this.props.show} clicked={this.props.modalClosed} />
        <div
          className={cssClasses.Modal}
          style={{
            transform: this.props.show ? "translateY(0)" : "translateY(-100vh)",
            opacity: this.props.show ? "1" : "0"
          }}
        >
          {this.props.children}
        </div>
      </Aux>
    );
  }
}

export default Modal;
