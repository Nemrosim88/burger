import React from "react";
import burgerCssClasses from "./Burger.css";
import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";

const burger = props => {
  let transformedIngredients = Object.keys(props.ingredients)
    .map(igKey => {
      console.log("First map. igKey", igKey);

      let keyValue = props.ingredients[igKey];
      console.log("First map. Key value -> ", keyValue);

      let newArray = [...Array(keyValue)];
      console.log("First map. New array -> ", newArray);

      return newArray.map((_, i) => {
        console.log("Second map. i", i);

        return <BurgerIngredient key={igKey + i} type={igKey} />;
      }); // [,]
    })
    // prevValue, currentValue {}, initialValue
    .reduce((arr, el) => {
      console.log("Arr -> ", arr);
      console.log("El -> ", el);
      return arr.concat(el);
    }, []);

  if (transformedIngredients.length === 0) {
    transformedIngredients = <p>Please start adding ingredients</p>
  }

  console.log("transformedIngredients", transformedIngredients);

  return (
    <div className={burgerCssClasses.Burger}>
      <BurgerIngredient type="bread-top" />
      {transformedIngredients}
      <BurgerIngredient type="bread-bottom" />
    </div>
  );
};
export default burger;
