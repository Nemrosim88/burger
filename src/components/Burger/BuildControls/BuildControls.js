import React from "react";
import cssClasses from "./BuildControls.css";
import BuildControl from "./BuildControl/BuildControl";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" }
];

const buildControls = props => (
  <div className={cssClasses.BuildControls}>
    <p>
      Current Price: <strong>{props.price.toFixed(2)}</strong>
    </p>
    {controls.map(ctrl => (
      <BuildControl
        key={ctrl.label}
        label={ctrl.label}
        // type={ctrl.type}
        // added={props.ingredientAdded}
        // TODO: Вместо этих двух строчек кода производим
        // замену на следующу строчку
        added={() => props.ingredientAdded(ctrl.type)}
        removed={() => props.ingredientRemoved(ctrl.type)}
        disabled={props.disabled[ctrl.type]}
      />
    ))}
    <button className={cssClasses.OrderButton}
    disabled={!props.purchaseable}
    onClick={props.ordered}>ORDER NOW</button>
  </div>
);

export default buildControls;
